#!/usr/bin/python
__author__ = "AXIA Studio"
__version__ = "0.1"

import sys
from optparse import OptionParser
from urlparse import urlparse, parse_qs
from BaseHTTPServer import BaseHTTPRequestHandler, HTTPServer

# sample usage:
#
# http://127.0.0.1:4701/?titolo=COMUNE+DI+MORI&servizio=Anagrafe+Stato+civile&data=05/02/2015&ora=10:35&coda=MORI....PUNTO+COMUNE&numero=0001&codiceservizio=A


################################
#
# cut and paste from totem.py
#
################################

VERTICALMODE = chr(27) + "&P" + chr(57) + chr(251)
RETRACTMODE = chr(27) + "&P" + chr(45) + chr(100)
SELECTFONT = chr(27) + chr(33) + chr(1)
CENTER = chr(27) + chr(78) + chr(1)
BOLDON = chr(27) + chr(66) + chr(1)
BOLDOFF = chr(27) + chr(66) + chr(0)
CENTER = chr(27) + chr(78) + chr(1)
NEWLINE = chr(10) + chr(13)
PRINTANDCUT = chr(30)
def printTicket(titolo, servizio, data, ora, coda, numero, sportello, codice):
    out = ''
    out += VERTICALMODE
    out += RETRACTMODE + SELECTFONT + CENTER + BOLDON
    out += chr(27) + chr(104) + chr(3) # text hight
    out += chr(27) + chr(119) + chr(1) # text width
    out += CENTER + '%s\n' % titolo
    out += BOLDOFF
    out += chr(27) + chr(104) + chr(0) # text hight
    out += chr(27) + chr(119) + chr(0) # text width
    #lines = servizio.split('\n')
    lines = servizio.split('\\n')
    for line in lines:
        out += CENTER + '%s\n' % line
    out += NEWLINE + NEWLINE
    out += '   SERVIZIO                    NUMERO\n'
    out += chr(27) + chr(104) + chr(3) # text hight
    out += chr(27) + chr(119) + chr(1) # text width
    out += "   %s           %s\n" % (codice, numero) + chr(27) + chr(112)
    out += chr(27) + chr(104) + chr(0) # text hight
    out += chr(27) + chr(119) + chr(0) # text width
    out += NEWLINE + NEWLINE
    out += '   %s                    %s\n' % (data, ora)
    out += CENTER + NEWLINE + NEWLINE
    out += CENTER + '%s\n' % coda
    out += PRINTANDCUT
    return out


global printer, filter

class RequestHandler(BaseHTTPRequestHandler):
    """
    The main request handler
    """

    def get501(self):
        self.send_response(501)
        self.send_header("Content-type", "text/html")
        self.end_headers()
        self.wfile.write("BAD")

    def do_GET(self):
        """
        Handler for the GET request
        """
        if filter != None and self.client_address[0] != filter:
            self.get501()
            return
        pars = parse_qs(urlparse(self.path).query)
        try:
            titolo = pars["titolo"][0]
            servizio = pars["servizio"][0]
            data = pars["data"][0]
            ora = pars["ora"][0]
            coda = pars["coda"][0]
            numero = pars["numero"][0]
            #sportello = pars["sportello"][0]
            sportello = None
            codiceservizio = pars["codiceservizio"][0]
        except KeyError:
            self.get501()
            return

        out = printTicket(titolo, servizio, data, ora, coda, numero, sportello, codiceservizio)
        f = open(printer, 'w')
        f.write(out)
        f.close()

        #self.send_response(200)
        #self.send_header("Content-type", "text/html")
        #self.end_headers()
        #self.end_headers("Access-Control-Allow-Origin", "*")

        self.send_response(200)
        self.send_header("Content-type", "text/html")
        self.send_header('Access-Control-Allow-Origin', '*')
        self.end_headers()

        self.wfile.write("OK")
        return





def main():
    """
    Entry point method: configure and start the http server
    :return: none
    """

    global printer, filter

    parser = OptionParser(usage="%prog [options] [config-name=value...]",
                          version="HTTP PRT SRV " + __version__)

    parser.add_option("-H",
                      "--host",
                      dest="host",
                      help="Http server host name or ip (default localhost)",
                      metavar="HOST",
                      default="localhost")

    parser.add_option("-P",
                      "--port",
                      type="int",
                      dest="port",
                      help="Http server port (default 4701)",
                      metavar="PORT",
                      default=4701)

    parser.add_option("-p",
                      "--printer",
                      dest="printer",
                      help="Printer (default LPT1:)",
                      metavar="LP",
                      default="LPT1:")

    parser.add_option("-F",
                      dest="filter",
                      help="Filters client by host name or ip (default none)",
                      metavar="HOST",
                      default=None)

    options, args = parser.parse_args()

    printer = options.printer
    filter = options.filter

    try:
        server = HTTPServer((options.host, options.port), RequestHandler)
        print "Started httpserver on %s:%d " % (options.host, options.port)
        server.serve_forever()

    except KeyboardInterrupt:
        print "^C received, shutting down the web server"
        server.socket.close()


if __name__ == "__main__":
    main()







