package com.axiastudio.gda.resources.protocollo;

import com.axiastudio.gda.entities.protocollo.Protocollo;
import com.axiastudio.zoefx.ws.rest.AbstractResource;

import javax.ws.rs.Path;

/**
 * User: tiziano
 * Date: 17/11/14
 * Time: 09:40
 */
@Path("protocolli")
public class ProtocolloResource extends AbstractResource<Protocollo> {

    @Override
    public Class getEntityClass() {
        return Protocollo.class;
    }

}
