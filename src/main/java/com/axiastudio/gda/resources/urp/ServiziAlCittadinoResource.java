package com.axiastudio.gda.resources.urp;

import com.axiastudio.gda.entities.urp.ServizioAlCittadino;
import com.axiastudio.zoefx.ws.rest.AbstractResource;

import javax.ws.rs.*;

/**
 * User: tiziano
 * Date: 17/11/14
 * Time: 09:40
 */
@Path("servizialcittadino")
public class ServiziAlCittadinoResource extends AbstractResource<ServizioAlCittadino> {

    @Override
    public Class getEntityClass() {
        return ServizioAlCittadino.class;
    }

}
