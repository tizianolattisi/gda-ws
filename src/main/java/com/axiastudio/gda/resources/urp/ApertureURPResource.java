package com.axiastudio.gda.resources.urp;

import com.axiastudio.gda.entities.urp.AperturaURP;
import com.axiastudio.zoefx.ws.rest.AbstractResource;

import javax.ws.rs.Path;

/**
 * User: tiziano
 * Date: 17/11/14
 * Time: 09:40
 */
@Path("apertureurp")
public class ApertureURPResource extends AbstractResource<AperturaURP> {

    @Override
    public Class getEntityClass() {
        return AperturaURP.class;
    }


}
