package com.axiastudio.gda.resources.urp;

import com.axiastudio.gda.entities.urp.Sportello;
import com.axiastudio.zoefx.ws.rest.AbstractResource;

import javax.ws.rs.Path;

/**
 * User: tiziano
 * Date: 17/11/14
 * Time: 09:40
 */
@Path("sportelli")
public class SportelliResource extends AbstractResource<Sportello> {

    @Override
    public Class getEntityClass() {
        return Sportello.class;
    }

}
