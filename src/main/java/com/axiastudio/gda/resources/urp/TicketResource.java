package com.axiastudio.gda.resources.urp;

import com.axiastudio.gda.entities.urp.Ticket;
import com.axiastudio.zoefx.ws.rest.AbstractResource;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.util.Date;

/**
 * User: tiziano
 * Date: 17/11/14
 * Time: 09:40
 */
@Path("ticket")
public class TicketResource extends AbstractResource<Ticket> {

    @Override
    public Class getEntityClass() {
        return Ticket.class;
    }

}
