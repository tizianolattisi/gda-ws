package com.axiastudio.gda.services.protocollazione;

/**
 * User: tiziano
 * Date: 06/05/15
 * Time: 10:07
 */
public class NuovaAttribuzioneRequest {

    private String ufficio;
    private Boolean principale=true;
    private Boolean lettaprecedente=true;

    public NuovaAttribuzioneRequest() {
    }

    public String getUfficio() {
        return ufficio;
    }

    public void setUfficio(String ufficio) {
        this.ufficio = ufficio;
    }

    public Boolean getPrincipale() {
        return principale;
    }

    public void setPrincipale(Boolean principale) {
        this.principale = principale;
    }

    public Boolean getLettaprecedente() {
        return lettaprecedente;
    }

    public void setLettaprecedente(Boolean lettaprecedente) {
        this.lettaprecedente = lettaprecedente;
    }
}
