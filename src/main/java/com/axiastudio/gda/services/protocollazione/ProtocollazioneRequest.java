package com.axiastudio.gda.services.protocollazione;

/**
 * User: tiziano
 * Date: 05/03/15
 * Time: 15:41
 */

public class ProtocollazioneRequest {

    private String oggetto;
    private String cfmittente;
    private String pivamittente;
    private String etremimittente;
    private String idmessaggio;
    private String progressivodocumento;
    private String progressivoallegato;
    private String ufficiodestinatario;

    public ProtocollazioneRequest() {
    }

    public String getOggetto() {
        return oggetto;
    }

    public void setOggetto(String oggetto) {
        this.oggetto = oggetto;
    }

    public String getCfittente() {
        return cfmittente;
    }

    public void setCfmittente(String cfamittente) {
        this.cfmittente = cfmittente;
    }

    public String getPivamittente() {
        return pivamittente;
    }

    public void setPivamittente(String pivamittente) {
        this.pivamittente= pivamittente;
    }

    public String getEtremimittente() {
        return etremimittente;
    }

    public void setEtremimittente(String etremimittente) {
        this.etremimittente = etremimittente;
    }

    public String getIdmessaggio() {
        return idmessaggio;
    }

    public void setIdmessaggio(String idmessaggio) {
        this.idmessaggio = idmessaggio;
    }

    public String getProgressivodocumento() {
        return progressivodocumento;
    }

    public void setProgressivodocumento(String progressivodocumento) {
        this.progressivodocumento = progressivodocumento;
    }

    public String getProgressivoallegato() {
        return progressivoallegato;
    }

    public void setProgressivoallegato(String progressivoallegato) {
        this.progressivoallegato = progressivoallegato;
    }

    public String getUfficiodestinatario() {
        return ufficiodestinatario;
    }

    public void setUfficiodestinatario(String ufficiodestinatario) {
        this.ufficiodestinatario = ufficiodestinatario;
    }
}
