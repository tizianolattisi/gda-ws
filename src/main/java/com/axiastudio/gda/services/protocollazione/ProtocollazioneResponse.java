package com.axiastudio.gda.services.protocollazione;

import com.axiastudio.zoefx.ws.rest.json.ResourceSupport;

import java.util.Date;

/**
 * User: tiziano
 * Date: 18/03/15
 * Time: 20:57
 */
public class ProtocollazioneResponse extends ResourceSupport {

    private Long id;
    private String iddocumento;
    private Date dataprotocollo;
    private Integer anno;


    public String getIddocumento() {
        return iddocumento;
    }

    public void setIddocumento(String iddocumento){
        this.iddocumento = iddocumento;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDataprotocollo() {
        return dataprotocollo;
    }

    public void setDataprotocollo(Date dataprotocollo) {
        this.dataprotocollo = dataprotocollo;
    }

    public Integer getAnno() {
        return anno;
    }

    public void setAnno(Integer anno) {
        this.anno = anno;
    }
}
