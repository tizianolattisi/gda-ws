package com.axiastudio.gda.services.protocollazione;

/**
 * User: tiziano
 * Date: 05/03/15
 * Time: 15:41
 */

public class AnnullamentoProtocolloRequest {

    private String oggetto;
    private String mittente;
    private String etremimittente;
    private String idmessaggio;
    private String progressivodocumento;
    private String progressivoallegato;
    private String ufficiodestinatario;

    public AnnullamentoProtocolloRequest() {
    }

    public String getOggetto() {
        return oggetto;
    }

    public void setOggetto(String oggetto) {
        this.oggetto = oggetto;
    }

    public String getMittente() {
        return mittente;
    }

    public void setMittente(String mittente) {
        this.mittente = mittente;
    }

    public String getEtremimittente() {
        return etremimittente;
    }

    public void setEtremimittente(String etremimittente) {
        this.etremimittente = etremimittente;
    }

    public String getIdmessaggio() {
        return idmessaggio;
    }

    public void setIdmessaggio(String idmessaggio) {
        this.idmessaggio = idmessaggio;
    }

    public String getProgressivodocumento() {
        return progressivodocumento;
    }

    public void setProgressivodocumento(String progressivodocumento) {
        this.progressivodocumento = progressivodocumento;
    }

    public String getProgressivoallegato() {
        return progressivoallegato;
    }

    public void setProgressivoallegato(String progressivoallegato) {
        this.progressivoallegato = progressivoallegato;
    }

    public String getUfficiodestinatario() {
        return ufficiodestinatario;
    }

    public void setUfficiodestinatario(String ufficiodestinatario) {
        this.ufficiodestinatario = ufficiodestinatario;
    }
}
