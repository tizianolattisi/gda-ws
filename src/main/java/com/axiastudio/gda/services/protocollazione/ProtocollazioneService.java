package com.axiastudio.gda.services.protocollazione;

import com.axiastudio.gda.entities.anagrafiche.Soggetto;
import com.axiastudio.gda.entities.base.Ufficio;
import com.axiastudio.gda.entities.finanziaria.Servizio;
import com.axiastudio.gda.entities.pratiche.Pratica;
import com.axiastudio.gda.entities.protocollo.*;
import com.axiastudio.zoefx.ws.rest.RestHelper;
import com.axiastudio.zoefx.core.IOC;
import com.axiastudio.zoefx.core.db.Database;
import com.axiastudio.zoefx.core.db.Manager;
import com.axiastudio.zoefx.ws.rest.json.Link;

import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.net.URI;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * User: tiziano
 * Date: 05/03/15
 * Time: 15:25
 */
@Path("protocollazione")
public class ProtocollazioneService {

    @PUT
    @Path("{iddocumento}/nuovaattribuzione")
    public Response put(NuovaAttribuzioneRequest request,
                        @PathParam("iddocumento") String iddocumento, @Context UriInfo uriInfo){
        URI uri = uriInfo.getAbsolutePathBuilder().build();

        Database db = IOC.queryUtility(Database.class);

        Manager<Servizio> manager = db.createManager(Servizio.class);
        Map<String, Object> map = new HashMap<>();
        map.put("responsabileprocedura", request.getUfficio());
        List<Servizio> servizi = manager.query(map);
        if( servizi.size()!=1 ){
            return Response.status(Response.Status.BAD_REQUEST)
                    .entity("Deve essere presente un servizio (e uno solo) associato al responsabile di procedura " + request.getUfficio() + ".")
                    .link(uri, "self").build();
        }
        Servizio servizio = servizi.get(0);

        Ufficio ufficio = servizio.getAttribuzione();
        if( ufficio==null ){
            return Response.status(Response.Status.BAD_REQUEST)
                    .entity("Al servizio " + servizio.getDescrizione() + " non è associata un'attribuzione valida.")
                    .link(uri, "self").build();
        }
        Manager<Protocollo> protocolloManager = db.createManager(Protocollo.class);
        Map<String, Object> map2 = new HashMap<>();
        map2.put("iddocumento", iddocumento);
        List<Protocollo> rs = protocolloManager.query(map2);
        Protocollo protocollo;
        if( rs.size()== 1 ) {
            protocollo = rs.get(0);
        } else {
            return Response.status(Response.Status.BAD_REQUEST)
                    .entity("Impossibile trovare il protocollo " + iddocumento + ".")
                    .link(uri, "self").build();
        }

        Collection<Attribuzione> attribuzioni = protocollo.getAttribuzioneCollection();
        for( Attribuzione attribuzione: attribuzioni ){
            if( attribuzione.getUfficio().equals(ufficio) ){
                // attribuzione presente
                return Response
                        .status(Response.Status.ACCEPTED)
                        .link(uri, "self").build();
            }
        }
        Attribuzione nuovaAttribuzione = new Attribuzione();

        if( request.getPrincipale() ){
            for( Attribuzione attribuzione: attribuzioni ){
                if( request.getLettaprecedente() ){
                    attribuzione.setLetto(true);
                }
                if( attribuzione.getPrincipale() ) {
                    attribuzione.setPrincipale(false);
                }
            }
            nuovaAttribuzione.setPrincipale(true);
        }

        nuovaAttribuzione.setIddocumento(iddocumento);
        nuovaAttribuzione.setUfficio(ufficio);
        attribuzioni.add(nuovaAttribuzione);
        protocollo.setAttribuzioneCollection(attribuzioni);
        protocolloManager.save(protocollo);

        Response response = Response
                .status(Response.Status.ACCEPTED)
                .link(uri, "self").build();
        return response;
    }

    @PUT
    @Path("{iddocumento}/{hash}/annullamento")
    public Response put(@PathParam("iddocumento") String iddocumento,
                        @PathParam("hash") String hash, @Context UriInfo uriInfo){

        URI uri = uriInfo.getAbsolutePathBuilder().build();
        Properties properties = IOC.queryUtility(Properties.class);

        String seed = properties.getProperty("seed");
        if( !md5Hash(iddocumento+seed).equals(hash) ){
            return Response.status(Response.Status.BAD_REQUEST)
                    .link(uri, "self").build();
        }

        // TODO: includere motivazione?
        Database db = IOC.queryUtility(Database.class);
        Manager<Protocollo> manager = db.createManager(Protocollo.class);
        Map<String, Object> map = new HashMap<>();
        map.put("iddocumento", iddocumento);
        List<Protocollo> rs = manager.query(map);
        if( rs.size()== 1 ){
            Protocollo protocollo = rs.get(0);
            protocollo.setAnnullato(Boolean.TRUE);
            manager.save(protocollo);
        }


        Response response = Response
                .status(Response.Status.ACCEPTED)
                .link(uri, "self").build();
        return response;
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response post(ProtocollazioneRequest richiesta, @Context UriInfo uriInfo){

        Properties properties = IOC.queryUtility(Properties.class);
        String base = properties.getProperty("doc_service_base_protocollo");
        String seed = properties.getProperty("seed");
        Long sportello_sdi = Long.parseLong(properties.getProperty("sportello_sdi"));
        Long destinatario_sdi = Long.parseLong(properties.getProperty("destinatario_sdi"));
        Long mittente_sdi = Long.parseLong(properties.getProperty("mittente_sdi"));

        Protocollo protocollo = new Protocollo();
        protocollo.setTipo(TipoProtocollo.ENTRATA);
        protocollo.setOggetto(richiesta.getOggetto());
        Database db = IOC.queryUtility(Database.class);
        Manager<Ufficio> ufficioManager = db.createManager(Ufficio.class);
        protocollo.setSportello(ufficioManager.get(sportello_sdi));

        Map<String, Object> map = new HashMap<>();
        long value = Long.parseLong(richiesta.getUfficiodestinatario());
        map.put("id", value);
        List<Ufficio> rs = ufficioManager.query(map);
        Ufficio ufficio;
        if (rs.size() == 1) {
            ufficio = rs.get(0);
        } else {
            ufficio = ufficioManager.get(destinatario_sdi);
        }
        List<Attribuzione> attribuzioni = new ArrayList<>();
        Attribuzione attribuzione = new Attribuzione();
        attribuzione.setUfficio(ufficio);
        attribuzione.setPrincipale(Boolean.TRUE);
        attribuzioni.add(attribuzione);
        protocollo.setAttribuzioneCollection(attribuzioni);

        List<UfficioProtocollo> ufficiProtocollo = new ArrayList<>();
        UfficioProtocollo ufficioProtocollo = new UfficioProtocollo();
        ufficioProtocollo.setUfficio(ufficio);
        ufficiProtocollo.add(ufficioProtocollo);
        protocollo.setUfficioProtocolloCollection(ufficiProtocollo);

        Manager<Soggetto> soggettoManager = db.createManager(Soggetto.class);
        List<SoggettoProtocollo> soggettiProtocollo = new ArrayList<>();
        SoggettoProtocollo soggettoSdiProtocollo = new SoggettoProtocollo();
        soggettoSdiProtocollo.setSoggetto(soggettoManager.get(mittente_sdi));
        soggettoSdiProtocollo.setPrincipale(Boolean.TRUE);
        soggettoSdiProtocollo.setPrimoinserimento(Boolean.TRUE);
        soggettiProtocollo.add(soggettoSdiProtocollo);

        Map<String, Object> map2 = new HashMap<>();
        map2.put("partitaiva", richiesta.getPivamittente());
        List<Soggetto> rs2 = soggettoManager.query(map2);
        Soggetto soggetto;
        if( rs2.size()==1 ) {
            soggetto = rs2.get(0);
            SoggettoProtocollo soggettoProtocollo = new SoggettoProtocollo();
            soggettoProtocollo.setSoggetto(soggetto);
            soggettoProtocollo.setPrincipale(Boolean.TRUE);
            soggettoProtocollo.setPrimoinserimento(Boolean.TRUE);
            soggettiProtocollo.add(soggettoProtocollo);
        }
        protocollo.setSoggettoProtocolloCollection(soggettiProtocollo);

        // pratica
        if( properties.getProperty("pratica_sdi_id") != null ) {
            Manager<Pratica> praticaManager = db.createManager(Pratica.class);
            Manager<Oggetto> oggettoManager = db.createManager(Oggetto.class);
            List<PraticaProtocollo> praticheProtocollo = new ArrayList<>();
            PraticaProtocollo praticaProtocollo = new PraticaProtocollo();
            praticaProtocollo.setPratica(praticaManager.get(Long.parseLong(properties.getProperty("pratica_sdi_id"))));
            if( properties.getProperty("oggetto_sdi_id") != null ) {
                praticaProtocollo.setOggetto(oggettoManager.get(Long.parseLong(properties.getProperty("oggetto_sdi_id"))));
            }
            praticaProtocollo.setOriginale(true);
            praticheProtocollo.add(praticaProtocollo);
            protocollo.setPraticaProtocolloCollection(praticheProtocollo);
        }

        Manager<Protocollo> manager = db.createManager(Protocollo.class);
        Protocollo saved = manager.save(protocollo);

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd/");
        String folderPath = base + dateFormat.format(saved.getDataprotocollo()) + saved.getIddocumento();

        String hash = md5Hash(saved.getIddocumento() + seed);
        String documentservice = folderPath + "/" + hash + "/documento";
        String annullamentoservice = uriInfo.getAbsolutePathBuilder().build() + "/" + saved.getIddocumento() + "/" + hash + "/annullamento";

        ProtocollazioneResponse protocollazioneResponse = new ProtocollazioneResponse();
        //protocollazioneResponse.setProtocollo(saved);
        protocollazioneResponse.setIddocumento(saved.getIddocumento());
        protocollazioneResponse.setId(saved.getId());
        protocollazioneResponse.setAnno(saved.getAnno());
        protocollazioneResponse.setDataprotocollo(saved.getDataprotocollo());
        protocollazioneResponse.addLink(new Link("documentservice", documentservice));
        protocollazioneResponse.addLink(new Link("annullamentoprotocollo", annullamentoservice));
        return RestHelper.POST(protocollazioneResponse, uriInfo);
    }

    private String md5Hash(String s) {
        MessageDigest md;
        try {
            md = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            return "00000000000000000000000000000000";
        }
        md.update(s.getBytes());
        byte[] digest = md.digest();
        StringBuffer sb = new StringBuffer();
        for( int i=0; i<digest.length; i++ ){
            sb.append(Integer.toString((digest[i] & 0xff) + 0x100, 16).substring(1));
        }
        return sb.toString();
    }

}
