package com.axiastudio.gda.services.protocollazione2;


/**
 * User: tiziano
 * Date: 02/04/15
 * Time: 09:13
 */
public class SoggettoInRequest {
    private Long id;
    private Long titolo;
    private Long soggettoreferente;
    private Boolean conoscenza=false;
    private Boolean notifica=false;
    private Boolean corrispondenza=false;
    private Boolean principale=false;
    private Boolean primoinserimento=false;
    private Boolean pec=false;

    public SoggettoInRequest() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getTitolo() {
        return titolo;
    }

    public void setTitolo(Long titolo) {
        this.titolo = titolo;
    }

    public Long getSoggettoreferente() {
        return soggettoreferente;
    }

    public void setSoggettoreferente(Long soggettoreferente) {
        this.soggettoreferente = soggettoreferente;
    }

    public Boolean isConoscenza() {
        return conoscenza;
    }

    public void setConoscenza(Boolean conoscenza) {
        this.conoscenza = conoscenza;
    }

    public Boolean isNotifica() {
        return notifica;
    }

    public void setNotifica(Boolean notifica) {
        this.notifica = notifica;
    }

    public Boolean isCorrispondenza() {
        return corrispondenza;
    }

    public void setCorrispondenza(Boolean corrispondenza) {
        this.corrispondenza = corrispondenza;
    }

    public Boolean isPrincipale() {
        return principale;
    }

    public void setPrincipale(Boolean principale) {
        this.principale = principale;
    }

    public Boolean isPrimoinserimento() {
        return primoinserimento;
    }

    public void setPrimoinserimento(Boolean primoinserimento) {
        this.primoinserimento = primoinserimento;
    }

    public Boolean getPec() {
        return pec;
    }

    public void setPec(Boolean pec) {
        this.pec = pec;
    }
}
