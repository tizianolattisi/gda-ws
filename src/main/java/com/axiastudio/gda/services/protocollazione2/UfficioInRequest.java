package com.axiastudio.gda.services.protocollazione2;


/**
 * User: tiziano
 * Date: 02/04/15
 * Time: 09:14
 */
public class UfficioInRequest {
    private Long id;

    public UfficioInRequest() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

}
