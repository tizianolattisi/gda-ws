package com.axiastudio.gda.services.protocollazione2;

import com.axiastudio.gda.entities.anagrafiche.Soggetto;
import com.axiastudio.gda.entities.base.Ufficio;
import com.axiastudio.gda.entities.pratiche.Pratica;
import com.axiastudio.gda.entities.protocollo.Attribuzione;
import com.axiastudio.gda.entities.protocollo.*;
import com.axiastudio.zoefx.core.IOC;
import com.axiastudio.zoefx.core.db.Database;
import com.axiastudio.zoefx.core.db.Manager;
import com.axiastudio.zoefx.ws.rest.RestHelper;
import com.axiastudio.zoefx.ws.rest.json.Link;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * User: tiziano
 * Date: 05/03/15
 * Time: 15:25
 */
@Path("protocollazione2")
public class ProtocollazioneService {


    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response post(ProtocollazioneRequest request, @Context UriInfo uriInfo){

        Properties properties = IOC.queryUtility(Properties.class);
        String base = properties.getProperty("doc_service_base_protocollo");
        String seed = properties.getProperty("seed");

        Protocollo protocollo = new Protocollo();
        protocollo.setTipo(TipoProtocollo.valueOf(request.getTipo()));
        protocollo.setOggetto(request.getOggetto());
        protocollo.setTiporiferimentomittente(TipoRiferimentoMittente.valueOf(request.getRif_mitt_tipo()));

        Database db = IOC.queryUtility(Database.class);
        Manager<Ufficio> ufficioManager = db.createManager(Ufficio.class);
        protocollo.setSportello(ufficioManager.get(request.getSportello()));

        // soggetti
        Manager<Soggetto> soggettoManager = db.createManager(Soggetto.class);
        Manager<Titolo> titoloManager = db.createManager(Titolo.class);
        List<SoggettoProtocollo> soggettiProtocollo = new ArrayList<>();
        for( SoggettoInRequest soggettoInRequest: request.getSoggetti() ){
            SoggettoProtocollo soggettoProtocollo = new SoggettoProtocollo();
            soggettoProtocollo.setSoggetto(soggettoManager.get(soggettoInRequest.getId()));
            if( soggettoInRequest.getTitolo()!=null ) {
                soggettoProtocollo.setTitolo(titoloManager.get(soggettoInRequest.getTitolo()));
            }
            soggettoProtocollo.setConoscenza(soggettoInRequest.isConoscenza());
            soggettoProtocollo.setNotifica(soggettoInRequest.isNotifica());
            soggettoProtocollo.setCorrispondenza(soggettoInRequest.isCorrispondenza());
            soggettoProtocollo.setPrincipale(soggettoInRequest.isPrincipale());
            soggettoProtocollo.setPrimoinserimento(soggettoInRequest.isPrimoinserimento());
            if( soggettoInRequest.getSoggettoreferente() != null ){
                soggettoProtocollo.setSoggettoReferente(soggettoManager.get(soggettoInRequest.getSoggettoreferente()));
            }
            if( soggettoInRequest.getPec()!=null ){
                soggettoProtocollo.setPec(soggettoInRequest.getPec());
            }
            soggettiProtocollo.add(soggettoProtocollo);
        }
        protocollo.setSoggettoProtocolloCollection(soggettiProtocollo);

        // uffici
        List<UfficioProtocollo> ufficiProtocollo = new ArrayList<>();
        for( UfficioInRequest ufficioInRequest: request.getUffici() ){
            UfficioProtocollo ufficioProtocollo = new UfficioProtocollo();
            ufficioProtocollo.setUfficio(ufficioManager.get(ufficioInRequest.getId()));
            ufficiProtocollo.add(ufficioProtocollo);
        }
        protocollo.setUfficioProtocolloCollection(ufficiProtocollo);

        // attribuzioni
        List<Attribuzione> attribuzioni = new ArrayList<>();
        for( AttribuzioneInRequest attribuzioneInRequest: request.getAttribuzioni() ){
            Attribuzione attribuzione = new Attribuzione();
            attribuzione.setUfficio(ufficioManager.get(attribuzioneInRequest.getId()));
            attribuzione.setPrincipale(attribuzioneInRequest.isPrincipale());
            attribuzioni.add(attribuzione);
        }
        protocollo.setAttribuzioneCollection(attribuzioni);

        // pratica
        if( request.getPratica()!=null ) {
            Manager<Pratica> praticaManager = db.createManager(Pratica.class);
            Manager<Oggetto> oggettoManager = db.createManager(Oggetto.class);
            List<PraticaProtocollo> praticheProtocollo = new ArrayList<>();
            PraticaProtocollo praticaProtocollo = new PraticaProtocollo();
            praticaProtocollo.setPratica(praticaManager.get(request.getPratica()));
            if( request.getOggettopratica()!=null ){
                praticaProtocollo.setOggetto(oggettoManager.get(request.getOggettopratica()));
            }
            praticaProtocollo.setOriginale(true);
            praticheProtocollo.add(praticaProtocollo);
            protocollo.setPraticaProtocolloCollection(praticheProtocollo);
        }

        // convalide
        protocollo.setConvalidaattribuzioni(request.isConvalidaattribuzioni());
        protocollo.setConvalidaprotocollo(request.isConvalidaprotocollo());
        protocollo.setConsolidadocumenti(request.isConsolidadocumenti());

        Manager<Protocollo> manager = db.createManager(Protocollo.class);
        Protocollo saved = manager.save(protocollo);

        PecProtocollo pecProtocollo = new PecProtocollo();
        pecProtocollo.setProtocollo(saved);
        pecProtocollo.setBody(request.getBody());
        pecProtocollo.setSegnatura(request.getSegnatura());
        db.createManager(PecProtocollo.class).save(pecProtocollo);

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd/");
        String folderPath = base + dateFormat.format(saved.getDataprotocollo()) + saved.getIddocumento();

        String hash = md5Hash(saved.getIddocumento() + seed);
        String documentservice = folderPath + "/" + hash + "/documento";
        String annullamentoservice = uriInfo.getAbsolutePathBuilder().build() + "/" + saved.getIddocumento() + "/" + hash + "/annullamento";

        ProtocollazioneResponse protocollazioneResponse = new ProtocollazioneResponse();
        //protocollazioneResponse.setProtocollo(saved);
        protocollazioneResponse.setIddocumento(saved.getIddocumento());
        protocollazioneResponse.addLink(new Link("documentservice", documentservice));
        protocollazioneResponse.addLink(new Link("annullamentoprotocollo", annullamentoservice));
        return RestHelper.POST(protocollazioneResponse, uriInfo);
    }

    private String md5Hash(String s) {
        MessageDigest md;
        try {
            md = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            return "00000000000000000000000000000000";
        }
        md.update(s.getBytes());
        byte[] digest = md.digest();
        StringBuffer sb = new StringBuffer();
        for( int i=0; i<digest.length; i++ ){
            sb.append(Integer.toString((digest[i] & 0xff) + 0x100, 16).substring(1));
        }
        return sb.toString();
    }

}
