package com.axiastudio.gda.services.protocollazione2;

import com.axiastudio.zoefx.ws.rest.json.ResourceSupport;

import java.util.Date;

/**
 * User: tiziano
 * Date: 18/03/15
 * Time: 20:57
 */
public class ProtocollazioneResponse extends ResourceSupport {

    private String iddocumento;


    public String getIddocumento() {
        return iddocumento;
    }

    public void setIddocumento(String iddocumento){
        this.iddocumento = iddocumento;
    }

}
