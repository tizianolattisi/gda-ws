package com.axiastudio.gda.services.protocollazione2;

/**
 * User: tiziano
 * Date: 02/04/15
 * Time: 09:14
 */
public class AttribuzioneInRequest {
    private Long id;
    private Boolean principale;

    public AttribuzioneInRequest() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean isPrincipale() {
        return principale;
    }

    public void setPrincipale(Boolean principale) {
        this.principale = principale;
    }

}
