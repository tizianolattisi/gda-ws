package com.axiastudio.gda.services.protocollazione2;

import java.util.List;

/**
 * User: tiziano
 * Date: 05/03/15
 * Time: 15:41
 */

public class ProtocollazioneRequest {

    private String oggetto;
    private String body;
    private String segnatura;
    private String tipo;
    private Long sportello;
    private Long pratica;
    private Long oggettopratica;
    private String rif_mitt;
    private String rif_mitt_tipo;
    private String rif_mitt_data;
    private List<SoggettoInRequest> soggetti;
    private List<UfficioInRequest> uffici;
    private List<AttribuzioneInRequest> attribuzioni;
    private Boolean convalidaattribuzioni=false;
    private Boolean convalidaprotocollo=false;
    private Boolean consolidadocumenti=false;

    public ProtocollazioneRequest() {
    }

    public String getOggetto() {
        return oggetto;
    }

    public void setOggetto(String oggetto) {
        this.oggetto = oggetto;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getSegnatura() {
        return segnatura;
    }

    public void setSegnatura(String segnatura) {
        this.segnatura = segnatura;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public Long getSportello() {
        return sportello;
    }

    public void setSportello(Long sportello) {
        this.sportello = sportello;
    }

    public Long getPratica() {
        return pratica;
    }

    public void setPratica(Long pratica) {
        this.pratica = pratica;
    }

    public Long getOggettopratica() {
        return oggettopratica;
    }

    public void setOggettopratica(Long oggettopratica) {
        this.oggettopratica = oggettopratica;
    }

    public String getRif_mitt() {
        return rif_mitt;
    }

    public void setRif_mitt(String rif_mitt) {
        this.rif_mitt = rif_mitt;
    }

    public String getRif_mitt_tipo() {
        return rif_mitt_tipo;
    }

    public void setRif_mitt_tipo(String rif_mitt_tipo) {
        this.rif_mitt_tipo = rif_mitt_tipo;
    }

    public String getRif_mitt_data() {
        return rif_mitt_data;
    }

    public void setRif_mitt_data(String rif_mitt_data) {
        this.rif_mitt_data = rif_mitt_data;
    }

    public List<SoggettoInRequest> getSoggetti() {
        return soggetti;
    }

    public void setSoggetti(List<SoggettoInRequest> soggetti) {
        this.soggetti = soggetti;
    }

    public List<UfficioInRequest> getUffici() {
        return uffici;
    }

    public void setUffici(List<UfficioInRequest> uffici) {
        this.uffici = uffici;
    }

    public List<AttribuzioneInRequest> getAttribuzioni() {
        return attribuzioni;
    }

    public void setAttribuzioni(List<AttribuzioneInRequest> attribuzioni) {
        this.attribuzioni = attribuzioni;
    }

    public Boolean isConvalidaattribuzioni() {
        return convalidaattribuzioni;
    }

    public void setConvalidaattribuzioni(Boolean convalidaattribuzioni) {
        this.convalidaattribuzioni = convalidaattribuzioni;
    }

    public Boolean isConvalidaprotocollo() {
        return convalidaprotocollo;
    }

    public void setConvalidaprotocollo(Boolean convalidaprotocollo) {
        this.convalidaprotocollo = convalidaprotocollo;
    }

    public Boolean isConsolidadocumenti() {
        return consolidadocumenti;
    }

    public void setConsolidadocumenti(Boolean consolidadocumenti) {
        this.consolidadocumenti = consolidadocumenti;
    }
}
