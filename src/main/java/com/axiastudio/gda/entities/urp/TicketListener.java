package com.axiastudio.gda.entities.urp;

import com.axiastudio.zoefx.core.db.Database;
import com.axiastudio.zoefx.core.db.Manager;
import com.axiastudio.zoefx.core.IOC;
import com.axiastudio.zoefx.persistence.Operator;

import javax.persistence.PrePersist;
import java.util.*;

/**
 * User: tiziano
 * Date: 07/01/15
 * Time: 11:45
 */
public class TicketListener {

    @PrePersist
    void prePersist(Ticket ticket) {

        if( ticket.getId() == null ) {

            // Timestamp di emissione
            Date tsemesso = new Date();
            ticket.setTsemesso(tsemesso);

            // Inizio giorno
            Calendar calendar = Calendar.getInstance();
            calendar.set(Calendar.HOUR_OF_DAY, 0);
            calendar.set(Calendar.MINUTE, 0);
            calendar.set(Calendar.MILLISECOND, 0);

            // progressivo
            Database db = IOC.queryUtility(Database.class);
            Manager<Ticket> manager = db.createManager(Ticket.class);

            Map<String, Object> map = new HashMap<>();
            List<Object> opAndValue = new ArrayList<>();
            opAndValue.add(Operator.ge);
            opAndValue.add(calendar.getTime());
            map.put("tsemesso", opAndValue);

            List<Ticket> rs = manager.query(map, "tsemesso", Boolean.TRUE, 1);
            if (rs.size() == 1) {
                ticket.setNumero(rs.get(0).getNumero() + 1);
            } else {
                ticket.setNumero(1);
            }

        }

    }

}
