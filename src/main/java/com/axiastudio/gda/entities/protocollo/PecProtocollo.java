/*
 * Copyright (C) 2012 AXIA Studio (http://www.axiastudio.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.axiastudio.gda.entities.protocollo;

import javax.persistence.*;
import java.io.Serializable;

/**
 *
 * @author Michela Piva - Comune di Riva del Garda
 */
@Entity
@Table(schema="PROTOCOLLO")
@SequenceGenerator(name="genpecprotocollo", sequenceName="protocollo.pecprotocollo_id_seq", initialValue=1, allocationSize=1)
public class PecProtocollo implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="genpecprotocollo")
    private Long id;
    @JoinColumn(name = "protocollo", referencedColumnName = "iddocumento")
    @OneToOne
    private Protocollo protocollo;
    @Column(name="body")
    private String body="";
    @Column(name="segnatura")
    private String segnatura="";

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Protocollo getProtocollo() {
        return protocollo;
    }

    public void setProtocollo(Protocollo protocollo) {
        this.protocollo = protocollo;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getSegnatura() {
        return segnatura;
    }

    public void setSegnatura(String segnatura) {
        this.segnatura = segnatura;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PecProtocollo)) {
            return false;
        }
        PecProtocollo other = (PecProtocollo) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return this.getBody();
    }
    
}
