package com.axiastudio.gda;

import com.axiastudio.zoefx.core.db.Database;
import com.axiastudio.zoefx.persistence.JPADatabaseImpl;
import com.axiastudio.zoefx.core.IOC;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * User: tiziano
 * Date: 18/11/14
 * Time: 17:17
 */
@ApplicationPath("/rest")
public class GDAApplication extends Application{

    public GDAApplication() {
        Properties properties = new Properties();
        InputStream propertiesStream = GDAApplication.class.getResourceAsStream("/gda-ws.properties");
        if( propertiesStream != null ) {
            try {
                properties.load(propertiesStream);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        IOC.registerUtility(properties, Properties.class);

        Map<String, String> jpaProps = new HashMap<>();
        jpaProps.put("hibernate.connection.username", properties.getProperty("username"));
        jpaProps.put("hibernate.connection.password", properties.getProperty("password"));
        jpaProps.put("hibernate.connection.url", properties.getProperty("url"));
        jpaProps.put("hibernate.dialect", "org.hibernate.dialect.PostgreSQL82Dialect");

        Database db = new JPADatabaseImpl(Boolean.FALSE);
        db.open(properties.getProperty("pu"), jpaProps);
        IOC.registerUtility(db, Database.class);

    }

}
