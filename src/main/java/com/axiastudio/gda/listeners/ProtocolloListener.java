/*
 * Copyright (C) 2012 AXIA Studio (http://www.axiastudio.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.axiastudio.gda.listeners;


import com.axiastudio.gda.entities.protocollo.Protocollo;
import com.axiastudio.zoefx.core.IOC;
import com.axiastudio.zoefx.core.db.Database;
import com.axiastudio.zoefx.core.db.Manager;

import javax.persistence.PrePersist;
import java.util.*;

/**
 *
 * @author Tiziano Lattisi <tiziano at axiastudio.it>
 */
public class ProtocolloListener {

    @PrePersist
    void prePersist(Object object) {

        Protocollo protocollo = (Protocollo) object;
        Calendar calendar = Calendar.getInstance();
        Integer year = calendar.get(Calendar.YEAR);
        Date today = calendar.getTime();

        // Generazione dell'iddocumento
        Map<String, Object> filters = new HashMap<>();
        filters.put("anno", year);
        Database db = IOC.queryUtility(Database.class);
        Manager<Protocollo> manager = db.createManager(Protocollo.class);
        List<Protocollo> query = manager.query(filters, "iddocumento", Boolean.TRUE, 1);
        protocollo.setAnno(year);
        String newIddocumento;
        if( query.size() == 0){
            newIddocumento = year+"00000001";
        } else {
            Integer i = Integer.parseInt(query.get(0).getIddocumento().substring(4));
            i++;
            newIddocumento = year+String.format("%08d", i);
        }
        protocollo.setIddocumento(newIddocumento);

        /* convalide */
        //this.convalide(protocollo, autenticato, today);
        this.convalide(protocollo, today);

    }

    //private void convalide(Protocollo protocollo, Utente autenticato, Date today){
    private void convalide(Protocollo protocollo, Date today){
        if( protocollo.getConvalidaattribuzioni() &&
                (protocollo.getEsecutoreconvalidaattribuzioni() == null || protocollo.getEsecutoreconvalidaattribuzioni().length() == 0) &&
                protocollo.getDataconvalidaattribuzioni() == null ){
            protocollo.setEsecutoreconvalidaattribuzioni("gda-ws");
            protocollo.setDataconvalidaattribuzioni(today);
        }
        if( protocollo.getConvalidaprotocollo()&&
                (protocollo.getEsecutoreconvalidaprotocollo() == null || protocollo.getEsecutoreconvalidaprotocollo().length() == 0) &&
                protocollo.getDataconvalidaprotocollo() == null ){
            protocollo.setEsecutoreconvalidaprotocollo("gda-ws");
            protocollo.setDataconvalidaprotocollo(today);
            // numero protocollo
        }
        if( protocollo.getConsolidadocumenti()&&
                (protocollo.getEsecutoreconsolidadocumenti() == null || protocollo.getEsecutoreconsolidadocumenti().length() == 0) &&
                protocollo.getDataconsolidadocumenti() == null ){
            protocollo.setEsecutoreconsolidadocumenti("gda-ws");
            protocollo.setDataconsolidadocumenti(today);
        }
    }

}
