package com.axiastudio.gda.writers;

import com.axiastudio.zoefx.ws.rest.json.JacksonMessageBodyWriter;
import com.axiastudio.gda.entities.urp.Sportello;

import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.ext.Provider;

/**
 * User: tiziano
 * Date: 19/11/14
 * Time: 11:02
 */
@Provider
@Produces(MediaType.APPLICATION_JSON)
public class SportelloMessageBodyWriter extends JacksonMessageBodyWriter<Sportello> {

}
