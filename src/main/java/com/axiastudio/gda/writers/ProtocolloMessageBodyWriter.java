package com.axiastudio.gda.writers;

import com.axiastudio.gda.entities.protocollo.Protocollo;
import com.axiastudio.zoefx.ws.rest.json.JacksonMessageBodyWriter;

import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.ext.Provider;

/**
 * User: tiziano
 * Date: 19/11/14
 * Time: 11:02
 */
@Provider
@Produces(MediaType.APPLICATION_JSON)
public class ProtocolloMessageBodyWriter extends JacksonMessageBodyWriter<Protocollo> {

}
