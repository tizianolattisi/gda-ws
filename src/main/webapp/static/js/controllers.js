/**
 * User: mirko
 * Date: 19/10/2014
 * Time: 18:08
*/

//Define an angular module for our app
var totemLuisa = angular.module('totemLuisa', ['ngAnimate']);

// variabili globali
var PRINTERSRV      = 'http://127.0.0.1:4701/';
var REST_BASEROOT   = 'rest/';
var STATIC_BASEROOR = 'static/';
var AUDIO_BEEP      = STATIC_BASEROOR + 'audio/beep.wav';

/*
 =========
 SERVIZI
 =========
 */

totemLuisa.service('JsonService', function($http) {
        this.recordset2array = function(arr, rs, onSuccess){
            arr.length = 0;
            angular.forEach(rs._links, function(linkData, idx) {
                $http
                    .get(linkData.href)
                    .success(function(entityData, status){

                        arr[idx] = entityData;

                        if (onSuccess) {
                            onSuccess( arr );
                        }

                    });
            });
        }
    });

/*
 =========
 FACTORY
 =========
*/

totemLuisa.factory('serviziService', function ($http, JsonService) {
    var servizialcittadino = [];
    var servizialcittadino_attivi = [];

    return {
        servizialcittadino: servizialcittadino,
        servizialcittadino_attivi: servizialcittadino_attivi,

        /*getServizioalcittadino_attivo: function (index) {
            return servizialcittadino_attivi[index];

        },*/
        load: function () {
            $http
                .get(REST_BASEROOT + 'servizialcittadino?orderBy=descrizione')
                .success(function (data, status) {
                    if ((status >= 200) && (status <= 299)) {
                        servizialcittadino.length = 0; // svuoto l'array
                        //servizialcittadino.push.apply(servizialcittadino, data)

                        // attenzione, la chiamata alla funzione onSuccess, verrà invocata n volte
                        JsonService.recordset2array(servizialcittadino, data, function(servizialcittadinoCompleto) {
                            servizialcittadino_attivi.length = 0; // svuoto l'array
                            for (serv in servizialcittadinoCompleto) {
                                for (sportello in servizialcittadinoCompleto[serv].sportelli) {
                                    if (servizialcittadinoCompleto[serv].sportelli[sportello].attivo) {
                                        servizialcittadino_attivi.push(servizialcittadinoCompleto[serv]);
                                        break;
                                    }
                                }
                            }
                        });
                        //while( servizialcittadino.length < data._links.length );

                    } else {
                        console.log('ERRORE SU CARICAMENTO LISTA SERVIZI AL CITTADINO' + ' (segue "status" e "data")');
                        console.log('  status: ' + status);
                        console.log('  data: '   + data);
                    }
                })
                .error(function(data, status) {
                    console.log('ERRORE SU CARICAMENTO LISTA SERVIZI AL CITTADINO' + ' (segue "status" e "data")');
                    console.log('  status: ' + status);
                    console.log('  data: '   + data);
                });
        }
    };
});

totemLuisa.factory('ticketService', function ($http, JsonService) {
    var ticketAperti = [];

    return {
        ticketAperti: ticketAperti,

        load: function ( onSuccess ) {

            var data = new Date();
            var anno = data.getFullYear();
            var mese = data.getMonth();
            var giorno = data.getDate();

            var dataFrom = new Date( anno, mese, giorno, 0,0,0,0);
            var dataTo   = new Date( anno, mese, giorno, 23,59,59,999);

            // filtro per "chiamato", "tschiamato" nella giornata in corso
            // ordinato inversamente per "tschiamato"
            // limitato a 7 item
            var url = REST_BASEROOT + 'ticket?filterBy=chiamato,tschiamato+gt+' + dataFrom.getTime() + ':date,tschiamato+le+' + dataTo.getTime() + ':date&orderBy=tschiamato:desc&size=7';

            $http
                .get(url)
                .success(function (data, status) {

                    if ((status >= 200) && (status <= 299)) {
                        // salvo l'ultimo ticket chiamato prima di ricaricare la lista ticket
                        var lastTicketCalled = undefined;
                        if (ticketAperti.length > 0) {
                            console.log('DEBUG ticketAperti[0] = ' + ticketAperti[0]);
                            lastTicketCalled = ticketAperti[0].numero;
                        }

                        // aggiorno la lista
                        ticketAperti.length = 0; // svuoto l'array
                        //ticketAperti.push.apply(ticketAperti, data)

                        // attenzione, la chiamata alla funzione onSuccess, verrà invocata n volte
                        JsonService.recordset2array(ticketAperti, data, function(ticketApertiCompleti) {
                            console.log('DEBUG ticketApertiCompleti = ' + ticketApertiCompleti);
                            if (onSuccess) {
                                onSuccess( (ticketApertiCompleti.length > 0) && (ticketApertiCompleti[0].numero != lastTicketCalled) );
                            }
                        });

                    } else {
                        console.log('ERRORE SU CARICAMENTO LISTA TICKET APERTI/CHIAMATI' + ' (segue "status" e "data")');
                        console.log('  status: ' + status);
                        console.log('  data: '   + data);
                    }
                })
                .error(function(data, status) {
                    console.log('ERRORE SU CARICAMENTO LISTA TICKET APERTI/CHIAMATI' + ' (segue "status" e "data")');
                    console.log('  status: ' + status);
                    console.log('  data: '   + data);
                });
        },

        getNewTicket: function ( idServizio, onSuccess, onFailure ) {
            $http
                .post(REST_BASEROOT + 'ticket', { "servizioalcittadino": { "id": idServizio } })
                .success(function(data, status) {

                    if ((status >= 200) && (status <= 299)) {
                        if (typeof data == 'object') {

                            if (onSuccess) {
                                onSuccess( data );
                            }
                        }
                    } else {
                        if (onFailure) {
                            onFailure();
                        }

                        console.log('ERRORE SU RICHIESTA NUOVO TICKET' + ' (segue "status" e "data")');
                        console.log('  status: ' + status);
                        console.log('  data: '   + data);
                    }
                })
                .error(function(data, status) {
                    if (onFailure) {
                        onFailure();
                    }

                    console.log('ERRORE SU RICHIESTA NUOVO TICKET' + ' (segue "status" e "data")');
                    console.log('  status: ' + status);
                    console.log('  data: '   + data);
                });

        },

        requestPrintTicket: function ( ticket ) {
            // http://127.0.0.1:8080/?titolo=COMUNE+DI+MORI&servizio=Anagrafe+Stato+civile&data=05/02/15&ora=10:35&coda=MORI....PUNTO+COMUNE&numero=0001&codiceservizio=A

            var date = new Date( ticket.tsemesso );
            var data = lpad(date.getDate(), 2, '0') + '/' + lpad(date.getMonth()+1, 2, '0') + '/' + date.getFullYear();
            var ora  = lpad(date.getHours(), 2, '0') + ':' + lpad(date.getMinutes(), 2, '0');

            var titolo         = 'COMUNE+DI+MORI';
            var coda           = 'MORI....PUNTO+COMUNE';

            var servizio       = ticket.servizioalcittadino.descrizione;
            servizio           = servizio.replace( ' ', '+');

            var numero         = lpad(ticket.numero, 4, '0');
            var codiceservizio = ticket.servizioalcittadino.codiceservizio;

            //var url = '/?titolo=COMUNE+DI+MORI&servizio=Anagrafe+Stato+civile&data=05/02/15&ora=10:35&coda=MORI....PUNTO+COMUNE&numero=0001&codiceservizio=A';
            var url = PRINTERSRV + '?titolo='+titolo+'&servizio='+servizio+'&data='+data+'&ora='+ora+'&coda='+coda+'&numero='+numero+'&codiceservizio='+codiceservizio;

            console.log('CALL: ' + url);

            // GET verso server di stampa python locale
            var invocation =  new XMLHttpRequest();
            if (invocation)  {
                invocation.open ( 'GET' , url ,  true ) ;
                //invocation.onreadystatechange = function() {console.log( invocation.readyState)} ;
                invocation.send ( ) ;
            }
        }
    };
});

totemLuisa.factory('newsService', function ($http, JsonService) {
    var news = [];

    return {
        news: news,

        load: function () {
            var data = new Date();

            $http
                .get(REST_BASEROOT + 'notizieurp?filterBy=iniziopubblicazione+le+' + data.getTime() + ':date,finepubblicazione+gt+' + data.getTime() + ':date&orderBy=iniziopubblicazione,id')
                .success(function (data, status) {

                    if ((status >= 200) && (status <= 299)) {
                        // verifico se effettivamente c'è un aggiornamento delle news
                        var i = news.length;
                        var flag = true;

                        // verifico se i due array (vecchio e nuovo) sono diversi
                        if (i != data.length) {
                            flag = false;
                        } else {
                            while (i--) {
                                if (news[i].descrizione !== data[i].descrizione)
                                    flag = false;
                            }
                        }

                        // se gli array sono diversi aggiorno.
                        if (flag == false) {
                            news.length = 0; // svuoto l'array
                            //news.push.apply(news, data)

                            JsonService.recordset2array(news, data);
                        }
                    } else {
                        console.log('ERRORE SU CARICAMENTO LISTA NEWS' + ' (segue "status" e "data")');
                        console.log('  status: ' + status);
                        console.log('  data: '   + data);
                    }
                })
                .error(function(data, status) {
                    console.log('ERRORE SU CARICAMENTO LISTA NEWS' + ' (segue "status" e "data")');
                    console.log('  status: ' + status);
                    console.log('  data: '   + data);
                });
        }
    };
});

totemLuisa.factory('apertureURPService', function ($http, JsonService) {
    var apertureURP = [];

    return {
        apertureURP: apertureURP,

        load: function () {
            $http
                .get(REST_BASEROOT + 'apertureurp?orderBy=giornosettimana,oraapertura,minutoapertura')
                .success(function (rsData, status) {
                    if ((status >= 200) && (status <= 299)) {
                        apertureURP.length = 0; // svuoto l'array
                        JsonService.recordset2array(apertureURP, rsData);
                    }
                })
                .error(function(data, status) {
                    console.log('ERRORE SU CARICAMENTO LISTA APERTURE URP' + ' (segue "status" e "data")');
                    console.log('  status: ' + status);
                    console.log('  data: '   + data);
                });
        }
    };
});

/*
 =========
 FILTRI
 =========
 */

totemLuisa.filter("lPad", function() {
    return function(number, params) {
        var num  = params["num"]  || 4;
        var char = params["char"] || "0";

        if (number !== null && number !== undefined) {
            var str = "" + number;
            while (str.length < num) str = char + str;
            return str;
        }
    };
});

totemLuisa.filter("min", function() {
    return function(_array, params) {
        var num  = params["num"] || 7;

        if (_array !== null && _array !== undefined) {
            while (_array.length < num) {
                _array.push('segnaposto_' + _array.length);
            }
            return _array;
        }
    };
});

totemLuisa.filter("br_split", function() {
    return function(str, params) {
        var _str = str || "";

        return _str.split( /\\n/g );
    };
});

/*
Gestione loadingScreen (protezione sulle chiamate ajax)
*/
totemLuisa.config(function ($provide, $httpProvider) {
    // Intercept http calls.
    $provide.factory('MyHttpInterceptor', function ($q) {

        var numLoadings = 0;
        var loadingScreen = $('#loadingScreen');

        var hide = function() {
            if (!(--numLoadings))
                loadingScreen.hide();
        };
        var show = function() {
            numLoadings++;
            loadingScreen.show();
        };

        return {
            // On request success
            request: function (config) {
                // console.log(config); // Contains the data about the request before it is sent.

                show();

                // Return the config or wrap it in a promise if blank.
                return config || $q.when(config);
            },

            // On request failure
            requestError: function (rejection) {
                // console.log(rejection); // Contains the data about the error on the request.

                hide();

                // Return the promise rejection.
                return $q.reject(rejection);
            },

            // On response success
            response: function (response) {
                // console.log(response); // Contains the data from the response.

                hide();

                // Return the response or promise.
                return response || $q.when(response);
            },

            // On response failture
            responseError: function (rejection) {
                // console.log(rejection); // Contains the data about the error.

                hide();

                // Return the promise rejection.
                return $q.reject(rejection);
            }
        };
    });

    // Add the interceptor to the $httpProvider.
    $httpProvider.interceptors.push('MyHttpInterceptor');
});

/*
==========
CONTROLLER
==========
*/

totemLuisa.controller("totemControllerMain", function ($scope, $http, $interval, apertureURPService, ticketService, serviziService) {

    $scope.init = function() {
        $scope.apertureURP = [];
        $scope.apertureURP = apertureURPService.apertureURP;
        apertureURPService.load();
        setTimeout(function() { console.log('  apertureURP: ' + $scope.apertureURP );  }, 2000);

        // refresh lista aperture URP ogni minuto (60000 msec)
        $interval( function() {
            apertureURPService.load();
        }, 60000);

        $scope.servizialcittadino = [];
        $scope.servizialcittadino_attivi = [];
        $scope.servizialcittadino = serviziService.servizialcittadino;
        $scope.servizialcittadino_attivi = serviziService.servizialcittadino_attivi;
        serviziService.load();

        // refresh lista servizi al cittadino ogni minuto (60000 msec)
        $interval( function() {
            serviziService.load();
        }, 60000);
    };

    $scope.getNewTicket = function( idServizio ) {
        if (idServizio == undefined) {
            console.log('getNewTicket: idServizio non indicato');
            return false;
        }

        // POPUP ATTENDERE BIGLIETTO
        $(".popup_attendere_biglietto").fadeIn(200);

        ticketService.getNewTicket(idServizio,
            // CALLBACK onSuccess
            function( ticket ) {

                // Attendo 1.5 sec e poi nascondo il popUp ATTENDERE IL BIGLIETTO
                $(".popup_attendere_biglietto").delay(1500).fadeOut(200, function() {
                    // In cascata visualizzo il popUp PRELEVARE IL BIGLIETTO
                    $(".popup_prelevare_biglietto").fadeIn(200, function() {
                        // Attendo altri 1.5 sec e poi nascondo il popUp PRELEVARE IL BIGLIETTO
                        $(".popup_prelevare_biglietto").delay(1500).fadeOut(200);
                    });
                });

                // stampo il ticket appena generato via APPLET QZ-PRINT
                // printTicket( ticket );

                // stampo il ticket appena generato via PrintSrv.py
                ticketService.requestPrintTicket( ticket );

            },
            // CALLBACK onFailure
            function() {
                $(".popup_attendere_biglietto").fadeOut(200);
                $(".popup_prelevare_biglietto").fadeOut(200);
            }
        );

    };

    $scope.checkTotemAttivo = function() {
        var date = new Date();
        var gg = date.getDay();
        var minuti = date.getHours() * 60 + date.getMinutes()

        var ret = false;

        for (index in $scope.apertureURP) {
            if ($scope.apertureURP[index].giornosettimana == gg) {
                var minApertura = $scope.apertureURP[index].oraapertura * 60 + $scope.apertureURP[index].minutoapertura - $scope.apertureURP[index].anticipoapertura;
                var minChiusura = $scope.apertureURP[index].orachiusura * 60 + $scope.apertureURP[index].minutochiusura - $scope.apertureURP[index].anticipochiusura;

                if ((minuti >= minApertura) && (minuti <= minChiusura)) {
                    ret = true;
                }
            }
        }

        return ret;
    }

    $scope.checkSportelliAttivo = function() {
        var date = new Date();
        var gg = date.getDay();
        var minuti = date.getHours() * 60 + date.getMinutes()

        var ret = false;

        for (index in $scope.apertureURP) {
            if ($scope.apertureURP[index].giornosettimana == gg) {
                var minApertura = $scope.apertureURP[index].oraapertura * 60 + $scope.apertureURP[index].minutoapertura;
                var minChiusura = $scope.apertureURP[index].orachiusura * 60 + $scope.apertureURP[index].minutochiusura;

                if ((minuti >= minApertura) && (minuti <= minChiusura)) {
                    ret = true;
                }
            }
        }

        return ret;
    }

    $scope.init();
});

totemLuisa.controller("monitorControllerMain", function ($scope, $http, $interval, ticketService) {

    $scope.init = function() {
        //$scope.external_page = '';

        // leggo il parametro "external_page" con url per new statiche
        window.location.search.substr(1).replace("?", "").split('&').forEach(function (item) {
            tmp = item.split("=");
            if (tmp[0] === 'external_page') {
                var url = decodeURIComponent(tmp[1]);

                console.log('URL: ' + tmp[1] );
                console.log('URL encoded: ' + encodeURIComponent(tmp[1]) );
                console.log('URL decoded: ' + decodeURIComponent(tmp[1]) );
                console.log('URL used: ' + url );

                $('#external_page').append('<iframe src="' + url + '"></iframe>');
            }
        });

        $scope.lastTicket = '';

        $scope.audio_beep = new Audio(AUDIO_BEEP);

        $scope.ticketAperti = [];
        $scope.ticketAperti = ticketService.ticketAperti;
        ticketService.load();

        // refresh lista ticket aperti ogni 3 secondi
        $interval( function() {
            ticketService.load( function(changed) {
                // verifico se c'è stato un aggiornamento di ticket, nel caso lancio un BEEP
                if (changed) {
                    $scope.audio_beep.play();
                }
            });
        }, 3000);
    };

    $scope.init();
});

totemLuisa.controller("monitorControllerNews", function ($scope, $http, $interval, newsService) {

    $scope.init = function() {

        $scope.currentIndex = 0;

        $scope.news = [];
        $scope.news = newsService.news;
        newsService.load();

        // setInterval: refresh lista news ogni minuto
        $interval( function() {
            newsService.load();
        }, 60000);

        // setInterval: ciclo sulle news disponibili
        $interval( function() {
            $scope.currentIndex = ($scope.currentIndex + 1) % $scope.news.length;
        }, 6000);
    };

    $scope.init();

});
