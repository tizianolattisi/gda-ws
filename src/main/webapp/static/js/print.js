/**
 * Created by mirko on 11/01/15.
 * Interfaccia applet JZebra (QZapplet, QZIndustries.com)
 */


/*
<applet id="qz" code="qz.PrintApplet.class" archive="./static/applet/qz-print.jar" width="1" height="1">
    <param name="jnlp_href" value="./static/applet/qz-print_jnlp.jnlp"><param name="cache_option" value="plugin"><param name="disable_logging" value="false"><param name="initial_focus" value="false"><param name="codebase_lookup" value="false">
</applet>
*/

 deployQZ();

/**
 * Deploys different versions of the applet depending on Java version.
 * Useful for removing warning dialogs for Java 6.  This function is optional
 * however, if used, should replace the <applet> method.  Needed to address
 * MANIFEST.MF TrustedLibrary=true discrepency between JRE6 and JRE7.
 */
function deployQZ() {
    var attributes = {
        id: "qz",
        code:'qz.PrintApplet.class',
        archive:'./static/applet/qz-print.jar',
        width:1,
        height:1
    };
    var parameters = {
        jnlp_href: './static/applet/qz-print_jnlp.jnlp',
        cache_option:'plugin',
        disable_logging:'false',
        initial_focus:'false'};

    if (deployJava.versionCheck("1.7+") == true) {
    } else if (deployJava.versionCheck("1.6+") == true) {
        delete parameters['jnlp_href'];
    }

    deployJava.runApplet(attributes, parameters, '1.5');
}

/**
 * Automatically gets called when applet has loaded.
 */
function qzReady() {
    // Setup our global qz object
    window["qz"] = document.getElementById('qz');

    if (qz) {
        try {
            console.log('qz version: ' + qz.getVersion());

        } catch(err) { // LiveConnect error, display a detailed meesage
            console.log("ERROR:  \nThe applet did not load correctly.  Communication to the " +
            "applet has failed, likely caused by Java Security Settings.  \n\n" +
            "CAUSE:  \nJava 7 update 25 and higher block LiveConnect calls " +
            "once Oracle has marked that version as outdated, which " +
            "is likely the cause.  \n\nSOLUTION:  \n  1. Update Java to the latest " +
            "Java version \n          (or)\n  2. Lower the security " +
            "settings from the Java Control Panel.");
        }
    }
}

/**
 * Returns whether or not the applet is not ready to print.
 * Displays an alert if not ready.
 */
function notReady() {
    console.log('Applet is not ready');

    // If applet is not loaded, display an error
    if (!isLoaded()) {
        return true;
    }
    // If a printer hasn't been selected, display a message.
    else if (!qz.getPrinter()) {
        console.log('Please select a printer first by using the "Detect Printer".');
        return true;
    }
    return false;
}

/**
 * Returns is the applet is not loaded properly
 */
function isLoaded() {
    // solo per test (necessario con l'uso del tag applet)
    // window["qz"] = document.getElementById('qz');

    if (!qz) {
        console.log('Error: Print plugin is NOT loaded!');
        return false;
    } else {
        try {
            if (!qz.isActive()) {
                console.log('Error: Print plugin is loaded but NOT active!');
                return false;
            }
        } catch (err) {
            console.log('Error: Print plugin is NOT loaded properly!');
            return false;
        }
    }


    return true;
}

/**
 * Automatically gets called when "qz.print()" is finished.
 */
function qzDonePrinting() {
    // Alert error, if any
    if (qz.getException()) {
        console.log('Error printing: ' + qz.getException().getLocalizedMessage());
        qz.clearException();
        return;
    }

    // Alert success message
    console.log('Successfully sent print data to "' + qz.getPrinter() + '" queue.');
}

function printTicket(ticket) {
    if (ticket == undefined) {
        return false;
    }

    //var servizio = 'COMUNE DI MORI';
    var servizio = ticket.servizioalcittadino.descrizione;

    var date = new Date( ticket.tsemesso );
    var data     = date.getDate() + '/' + (date.getMonth()+1) + '/' + date.getFullYear();
    var ora      = date.getHours() + ':' + lpad(date.getMinutes(), 2, '0');

    var codice   = ticket.servizioalcittadino.codiceservizio;
    var numero   = lpad(ticket.numero, 4, '0');

    // dati per confronto con file prodotto da versione totem in Python
    //data = '01/01/2015';
    //ora  = '22:54';
    //numero   = '0123';
    //codice   = 'Serv. 1';

    var TITOLO       = 'COMUNE DI MORI';
    var CODA         = 'MORI....PUNTO COMUNE';

    if (isLoaded()) {
        // FIND DEFAULT PRINTER
        qz.findPrinter();

        // Automatically gets called when "qz.findPrinter()" is finished.
        window['qzDoneFinding'] = function() {

            // Alert the printer name to user
            var printer = qz.getPrinter();
            console.log(printer !== null ? 'Default printer found: "' + printer + '"': 'Default printer not found');

            // aggiunta clear come tentativo di fix: non stampa la seconda etichetta
            //qz.clear();

            // FORCE ENCODING: default UTF-8 is not allowed
            qz.setEncoding("cp1252");

            qz.append(chr(27) + '&P' + chr(57) + chr(251)); // VERTICALMODE;
            qz.append(chr(27) + '&P' + chr(45) + chr(100)); // RETRACTMODE;
            qz.append(chr(27) +  chr(33) + chr(1));         // SELECTFONT;
            qz.append(chr(27) +  chr(78) + chr(1));         // CENTER;
            qz.append(chr(27) +  chr(66) + chr(1));         // BOLDON;
            qz.append(chr(27) + chr(104) + chr(3));         // text hight;
            qz.append(chr(27) + chr(119) + chr(1));         // text width;
            qz.append(chr(27) +  chr(78) + chr(1));         // CENTER;

            qz.append(TITOLO + '\n');

            qz.append(chr(27) +  chr(66)); // + chr(0);  // BOLDOFF
            qz.appendHex("x00");
            qz.append(chr(27) + chr(104)); // + chr(0); // text hight;
            qz.appendHex("x00");
            qz.append(chr(27) + chr(119)); // + chr(0); // text width;
            qz.appendHex("x00");

            var lines = servizio.split('\n');
            for (var i=0; i<lines.length; i++) {
                qz.append(chr(27) + chr(78) + chr(1)); // CENTER;
                qz.append(lines[i] + '\n');
            }

            qz.append(chr(10) + chr(13)); // NEWLINE;
            qz.append(chr(10) + chr(13)); // NEWLINE;

            qz.append('   SERVIZIO                    NUMERO\n');

            qz.append(chr(27) + chr(104) + chr(3)); // text hight;
            qz.append(chr(27) + chr(119) + chr(1)); // text width;

            qz.append('   ' + codice + '           ' + numero + '\n' + chr(27) + chr(112));

            qz.append(chr(27) + chr(104)); // + chr(0); // text hight;
            qz.appendHex("x00");
            qz.append(chr(27) + chr(119)); // + chr(0); // text width;
            qz.appendHex("x00");
            qz.append(chr(10) + chr(13)); // NEWLINE;
            qz.append(chr(10) + chr(13)); // NEWLINE;
            qz.append('   ' + data + '                    ' + ora + '\n');

            qz.append(chr(27) + chr(78) + chr(1)); // CENTER;
            qz.append(chr(10) + chr(13));          // NEWLINE;
            qz.append(chr(10) + chr(13));          // NEWLINE;
            qz.append(chr(27) + chr(78) + chr(1)); // CENTER;

            qz.append(CODA + '\n');

            qz.append(chr(30)); // PRINTANDCUT;

            // PRINT TO DEFAULT PRINTER
            qz.print();

            try {
                // PRINT TO FILE (M = Mac; L = Linux; W = Windows)
                if ((navigator.platform[0] == 'M') || (navigator.platform[0] == 'L')) {
                    var fileName = "/tmp/qz-print_last_ticket.txt";
                    console.log('Stampa su file: ' + fileName);
                    qz.printToFile(fileName);
                }
                if (navigator.platform[0] == 'W') {
                    var fileName = "C:\\qz-print_last_ticket.txt";
                    console.log('Stampa su file: ' + fileName);
                    qz.printToFile(fileName);
                }
            }
            catch(err) {
                console.log('ERROR on print to file: ' + err.message);
            }

            // Remove reference to this function
            window['qzDoneFinding'] = null;
        };
    }
};


