/**
 * Created by mirko on 05/02/15.
 */

var chr = function(num) {
    return String.fromCharCode(num);
};

var lpad = function(str, num, char) {
    // forzo a stringa nel caso mi fosse passato un numero
    var _str = '';
    if (str !== null && str !== undefined) {
        var _str = _str + str;
    }

    var _num  = num || 4;
    var _char = char || "0";

    while (_str.length < _num)
        _str = _char + _str;

    return _str;
}
